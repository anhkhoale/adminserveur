#!/bin/bash

# Import variables file
source variables.sh

# If command typed is not is the list, this function will return "cmd not found"
cmdNotFound() {
  echo "cmd not found"
}

# Authentification 
auth() {
  echo "username: "
  read user
  echo "password: "
  read psswd
  if [ $user != $username ] || [ $password != $psswd ]; then
    echo "wrong username or password"
    exit
  fi
}

# Check age 
age() {
  read age
  if [ $age -ge "18" ]; then
    echo "you are major" 
  else
    echo "you are minor"
  fi
}

# Find and remove file
removefile() {
  if [ -f "$1" ]; then
    rm $1
  else
    echo "file is not exist"
  fi
  echo 
}

# Find and remove directory
removedir() {
  if [ -d "$1" ]; then
    rmdir $1
  else
    echo "directory is not exist"
  fi
  echo 
}

# Description of our prompt
about() {
  echo "This application is called prompt magic create by Anh Khoa LE"
}

# This function returns author profile. It's meeee !!!
profile() {
  echo "First name: $first_name"
  echo "Last name: $last_name"
  echo "Age: $age"
  echo "Email: $email"
}

# Change a user passowrd
changepsswd() {
  echo "User to change password ?"
  read input
  sudo passwd $input
}

changedirectory() {
  if [ -d "$1" ]; then
    cd $1
  else
    echo "directory is not exist"
  fi
  echo
}

# This function returns hour
hour() {
  date "+%H:%M:%S"
}

# This function returns the version of ubuntu
version() {
  lsb_release -a
}

# This function open a file with nano
open() {
  nano $1
}

# Send a mail, subject and body are required
smtp() {
  echo "Mail send to"
  read mail
  echo "Mail subject"
  read subject
  echo "Mail body"
  read body
  echo $body | mail -s "$subject" -a "FROM:$sender" $mail
  echo "Mail sent, check your spam"
}

# Get the html source and past it a file
httpget() {
  echo "Give me a URL"
  read url
  echo "Creat a file to add HTML code, name file ?"
  read name_file
  touch $name_file
  curl -o $name_file $url
}

# Show all function that you can use
helpfunction(){
  cat README.md
}
# This function is the core of our prompt
cmd() {
  auth
  while [[ 1 ]]; do
    currdir=echo pwd
    echo "$currdir"
    read cmd arg1
    
    case "$cmd" in
        pwd ) pwd;;
        quit | exit | q ) exit 16252;;        
        ls ) ls $arg1 ;;
        help ) helpfunction;;
        age ) age;;
        rm ) removefile $arg1;;
        rmd | rmdir ) removedir $arg1;;
        about ) about;;
        profile ) profile;;
        passw ) changepsswd;;
        version | vers | --v ) version;;
        cd ) changedirectory $arg1;;
        hour ) hour;;
        open ) open $arg1;;
        httpget ) httpget $args1;;
        smtp ) smtp;;
        * ) cmdNotFound;;

    esac
  done
}

# This function call function cmd
main() {
    cmd
}

# Launch main 
main