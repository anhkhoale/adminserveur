# Ma Magic Prompt
## To access to the prompt you have to execute this command
./main.sh


List of commands you can use in this prompt
- help: It will show all commands that you can use
- ls: List direcitry content
- rm: Remove a file
- rmd or rmdir: Remove a directory
- about: Description about my magic prompt
- version or --v or vers: Ubuntu version
- age: Check your age
- quit or exit or q: Exit prompt
- profile: This command will show the informations about author
- passw: Change password
- cd: Change directory
- pwd: Print the current working directory
- hour: This function responds the question: "What time is it?"
- httpget: Put the html source from a url into a file
- smtp: Send a mail to your partner
- open: Open a file

